/**
 * NanoState - efficient, effective, and extensible state management
 * 
 * @copyright 2018 Sophos Software
 * @license MIT
 */

export type State = {
    [key: string]: any
}

export abstract class Context<S extends State> {
    private store: Store

    public constructor(protected readonly state: S) { }

    public registerTo(aStore: Store): void {
        if (this.store !== undefined) {
            throw new Error(`Duplicate registration: Context is already registered to a Store`)
        }
        this.store = aStore
    }

    protected async notify(event: string, payload?: EventPayload): Promise<void> {
        if (this.store !== undefined) {
            return this.store.trigger(event, payload)
        }
    }
}

export type ContextFactory<T extends Context<State>> = () => T

export type ContextMap = {
    [key: string]: Context<State> | ContextFactory<Context<State>>
}

export abstract class Subscription {
    public constructor(private readonly events: string[] = ['*']) { }

    public listensTo(id: string): boolean {
        return this.events.indexOf(id) >= 0 || this.events.indexOf('*') >= 0
    }

    public abstract async handle(event: Event<EventPayload>): Promise<void>
}

export type Event<P extends EventPayload> = {
    id: string,
    store: Store,
    payload?: P
}

export type EventPayload = {
    [key: string]: any
}

export class Store {
    public constructor(
        private readonly contextMap: ContextMap,
        private readonly subscriptions: Subscription[] = []
    ) {
        for (let key in contextMap) {
            let context = contextMap[key]
            if (this.isContext(context)) {
                (context as Context<State>).registerTo(this)
            }
        }
    }

    public getContext<T extends Context<State>>(key: string): T {
        let context = this.contextMap[key]
        
        if (context === undefined) {
            throw new RangeError(`Invalid context alias [${key}]`)
        }

        if (this.isContext(context)) {
            return <T>context;
        }
        
        context = (this.contextMap[key] as ContextFactory<T>)()
        context.registerTo(this)
        this.contextMap[key] = context
        
        return <T>context
    }

    public async trigger(event: string, payload?: EventPayload): Promise<void> {
        await Promise.all(this.subscriptions.map(async sub => {
            if (sub.listensTo(event)) {
                await sub.handle({ id: event, store: this, payload: payload })
            }
        }))
    }

    private isContext(context: Context<State> | ContextFactory<Context<State>>) {
        return (context as Context<State>).registerTo !== undefined
    }
}
