import { expect } from 'chai'
import { Context, State, Store, Subscription, Event, EventPayload } from '../src'
import { mock, verify, when, anything, instance } from 'ts-mockito'

describe('NanoState', () => {

    interface TestState extends State {
        value: number
    }

    class TestContext extends Context<TestState> {
        public constructor(aValue: number = 1) {
            super({ value: aValue })
        }

        public get Value(): number {
            return this.state.value
        }

        public increment(value: number = 1): void {
            const current = this.state.value
            this.state.value += value
            this.notify('ValueIncremented', { old: current, new: this.state.value })
        }
    }

    interface TestPayload extends EventPayload {
        amount: number
    }

    interface TestEvent extends Event<TestPayload> {
        payload: TestPayload
    }

    class TestSub extends Subscription {
        public constructor() {
            super(['test', 'inc'])
        }

        public async handle(event: TestEvent): Promise<void> {
            const ctx = event.store.getContext<TestContext>('a')
            ctx.increment(event.payload.amount)
        }
    }

    class MultSub extends Subscription {
        public constructor(private readonly factor: number) {
            super(['inc'])
        }

        public async handle(event: TestEvent): Promise<void> {
            const ctx = event.store.getContext<TestContext>('a')
            ctx.increment(event.payload.amount * this.factor)
        }
    }

    describe('Context', () => {
        let context: TestContext
        let store: Store

        beforeEach(() => {
            context = new TestContext()
            store = mock(Store)
        })

        it('can be registered to a store', () => {
            expect(context.registerTo(instance(store))).to.be.undefined
        })

        it('can only be registered once', () => {
            const storeInst = instance(store)
            context.registerTo(storeInst)
            expect(() => context.registerTo(storeInst)).to.throw(Error)
        })

        it('notifies store of changes', async () => {
            const event: string = 'ValueIncremented'
            when(store.trigger(event, anything())).thenResolve()

            context.registerTo(instance(store))
            context.increment()
            verify(await store.trigger(event, anything())).once()
        })
    })

    describe('Store', () => {
        let store: Store

        beforeEach(() => {
            store = new Store(
                {
                    'a': new TestContext(),
                    'b': () => new TestContext(2)
                },
                [new TestSub(), new MultSub(2)]
            )
        })

        it('throws on invalid context alias', () => {
            expect(() => store.getContext<TestContext>('foo')).to.throw('Invalid context alias [foo]')
        })

        it('gets a reqested context', () => {
            expect(store.getContext<TestContext>('a')).to.not.be.null
        })

        it('lazy loads a context', () => {
            expect(store.getContext<TestContext>('b')).to.not.be.null
        })

        it('triggers events', async () => {
            await store.trigger('test', { amount: 3 })
            const ctx = store.getContext<TestContext>('a')
            expect(ctx.Value).to.eq(4)
        })

        it('triggers all events', async () => {
            await store.trigger('inc', { amount: 2 })
            const ctx = store.getContext<TestContext>('a')
            expect(ctx.Value).to.eq(7)
        })
    })

})
